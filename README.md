# Terra CRUD

The purpose of this bootstrapped app is to be a playground for learning React with Terra components.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

Information about Terra can be found [here](https://engineering.cerner.com/terra-ui/home/terra-ui/index).

All styles are written using [Sass](https://sass-lang.com/guide).

## Prerequisites

1. Install [NVM](https://github.com/nvm-sh/nvm#installing-and-updating)
2. Install [Yarn](https://classic.yarnpkg.com/en/docs/install/#mac-stable)
3. Familiarity with [React](https://reactjs.org/tutorial/tutorial.html)

## Getting Started

To make sure you are using the correct version Node, run the following command to install/use the version of node specified in `.nvmrc`
```sh
nvm install
# or
nvm use

node -v
# => Now using node v12.18...
```

### Running the Server
Since this app is strictly used for practice purposes, it will only ever run in development mode.

Start the [Webpack](https://webpack.js.org/) server using the command:
```sh
yarn start
```
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits using **hot reloading.**<br />
The webpack configuration is managed by `react-scripts`.

## Development

### Storybook

### Tests

This project uses Jest as its testing framework.
In the project directory, you can run:

```sh
yarn test
```

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Lint

This project uses [ESLint](https://eslint.org/) for all Javascript and React code.
The ESLint configuration for this project is the [AirBnB](https://github.com/airbnb/javascript) configuration.

It uses [Stylelint](https://stylelint.io/) for the Sass code.

To see the lint report, either check the Browser console or run this command in the root directory:

```sh
yarn lint
```

This will give you a clear console output report of the lint errors and warnings. Some errors can be automatically resolved using the `--fix` flag.

### Code Standards

All components should adhere to the following file structure, and new code contributions should not need to affect files/folders outside the following:

```
.
└── src
    ├── components
    |   └── ComponentOrConcern.jsx
    ├── pages
    |   └── ParentComponent.jsx
    ├── styles
    |   └── ComponentOrConcern.scss # if needed
    |   └── ParentComponent.scss # if needed
    └── tests
            ├── ComponentOrConcern.test.jsx
            └── ParentComponent.test.jsx

```

ComponentOrConcern = An encapsulated portion of the DOM or app functionality.
ParentComponent = Mounts all the Components in a single page. One per page. Primary landing page for a workflow.
