import React from 'react';
import { injectIntl } from 'react-intl';
import ApplicationLayout from 'terra-application-layout';
import PropTypes from 'prop-types';
import Text from 'terra-text';
import Welcome from '../pages/Welcome';

// Core Application
const App = ({ intl }) => {
  // Build Name Config
  const accessoryName = (
    <Text>
      {intl.formatMessage({ id: 'components.header.healthe_insights' })}
    </Text>
  );
  const nameConfig = { accessory: accessoryName };

  // Build Routing Config
  const routingConfig = {
    content: {
      '/': {
        path: '/',
        component: {
          default: {
            componentClass: Welcome,
            props: {
              baseUrl: '/',
              menuName: { welcomeTitle: intl.formatMessage({ id: 'welcome.title' }) },
              message: intl.formatMessage({ id: 'welcome.message' })
            },
          },
        },
      },
    },
  };

  return (
    <>
      <ApplicationLayout
        indexPath="/"
        nameConfig={nameConfig}
        routingConfig={routingConfig}
      />
    </>
  );
};

App.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
};

export default injectIntl(App);
