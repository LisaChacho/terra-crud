import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Base from 'terra-base';
import { BrowserRouter } from 'react-router-dom';
import App from './components/App';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Base locale="en">
        <App />
      </Base>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
