import React from 'react';
import PropTypes from 'prop-types';

const Welcome = ({ message }) => {
  // eslint-disable-next-line no-console
  console.log('This is the Welcome component.');
  return <div>{message}</div>;
};

export default Welcome;

Welcome.propTypes = {
  message: PropTypes.string
};
