'use strict';

module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  plugins: [
    'react',
    'jsx-a11y',
    'import',
    'react-hooks',
  ],
  env: {
    browser: true,
    jest: true,
  },
  rules: {
    // Disabled this rule as any prop that is not given a default is given the
    // value undefined, thus making it redundant to give default props the value
    // of undefined
    'react/require-default-props': 'off',
    // Disable this rule as it has been marked as deprecated in jsx-a11y plugin
    // https://github.com/evcohen/eslint-plugin-jsx-a11y/releases/tag/v6.1.0
    'jsx-a11y/label-has-for': 'off',
    // Replaces jsx-a11y/label-has-for rule. By default, it wants inputs to be
    // both wrapped in a label and include a id/for attribute mapping with
    // label. This config updates the rule to require one or the other.
    'jsx-a11y/label-has-associated-control': [2, {
      assert: 'either'
    }],
    // Disabling this rule until this PR is merged / released
    // https://github.com/benmosher/eslint-plugin-import/pull/1104
    // ESLint import plugin is unable to determine that most all of the
    // dependencies are in ./node_modules/@healtheintent/app-sdk/node_modules
    // and erroneously logs an error
    'import/no-extraneous-dependencies': 0,
    // Set this rule to warn to ease transition to app-sdk eslint config
    'react/destructuring-assignment': 'warn',
    // Setup rules of hooks, see: https://reactjs.org/docs/hooks-rules.html#eslint-plugin
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    // Disabled to ease transition to app-sdk eslint config
    'comma-dangle': 'off'
  },
  globals: {
    shallow: true,
    render: true,
    mount: true,
  },
};
